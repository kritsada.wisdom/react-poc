import "./App.css";
import { useLocation } from "react-router-dom";
import { Link } from "react-router-dom";
import { Routes } from "react-router-dom";
import { Route } from "react-router-dom";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

const Home = () => {
  return (
    <div>
      <Link to="/info/new" state={{ isEdit: false }}>
        Add Info
      </Link>
      <br />
      <Link to="/info2" state={{ isEdit: false }}>
        Add Info
      </Link>
      <br />
      {["A", "B", "C", "D"].map((e, i) => {
        return (
          <div key={i}>
            <br />
            <Link
              to={`/info/${e}`}
              state={{ isEdit: true, data: `${e}-state` }}
            >
              Edit info {e}
            </Link>
          </div>
        );
      })}
      <br />
      {["A1", "B2", "C3", "D4"].map((e, i) => {
        return (
          <div key={i}>
            <br />
            <Link to={`/info2/${e}/edit`} state={{ isEdit: true }}>
              Edit info {e}
            </Link>
          </div>
        );
      })}
    </div>
  );
};

const Info = () => {
  let { state, pathname } = useLocation();
  const isEdit = !!state?.isEdit || pathname.split("/").pop() === "edit";
  console.log(pathname);
  const [info, setInfo] = useState("");
  const { data } = useParams();
  const handleSubmit = () => {
    if (state?.isEdit) {
      alert(`${info} Edited`);
    } else {
      alert(`${info} Added`);
    }
  };

  useEffect(() => {
    if (isEdit) setInfo(state?.data ? state.data : data || "");
  }, []);
  return (
    <div>
      {isEdit ? "Edit" : "Add"} Info
      <br />
      <input value={info} onChange={(e) => setInfo(e.target.value)} />
      <br />
      <button onClick={handleSubmit}>{isEdit ? "Edit" : "Add"}</button>
      <br />
      <Link to="/">Back</Link>
    </div>
  );
};

function App() {
  return (
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/info2" element={<Info />}>
        <Route index element={<Info />} />
        <Route path="/info2/:data/edit" element={<Info />} />
      </Route>
      <Route path="/info/new" element={<Info />} />
      <Route path="/info/:data" element={<Info />} />
    </Routes>
  );
}

export default App;
